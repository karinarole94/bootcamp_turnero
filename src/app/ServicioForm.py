from django import forms

class ServicioFormulario(forms.Form):
    servicio = forms.CharField(max_length= 50, required=True)
    descripcion = forms.CharField(max_length= 100, required=True)
    activo = forms.BooleanField(required= False)