from django import forms
from .models import Servicio, Prioridad, Nacionalidad, TipoDocumento

#Servicio
class ServicioModelForm(forms.ModelForm):
    
    class Meta:
        model = Servicio
        fields = ("servicio","descripcion", "activo")

    def validacion_servicio(self):
        servicio = self.cleaned_data.get("servicio")
        if servicio == "" or servicio == None:
            raise forms.ValidationError("El campo Servicio no puede quedar vacio")
        
    def validacion_descripcion(self):
        descripcion = self.cleaned_data.get("descripcion")
        if descripcion == "" or descripcion == None:
            raise forms.ValidationError("El campo Descripcion no puede quedar vacio")
        

class ServicioFormulario(forms.Form):
    servicio = forms.CharField(max_length= 50, required=True)
    descripcion = forms.CharField(max_length= 100, required=True)
    activo = forms.BooleanField(required= False)

#Prioridad      
class PrioridadModelForm(forms.ModelForm):
    
    class Meta:
        model = Prioridad 
        fields = ("prioridad","condicion", "activo")

    def validacion_prioridad(self):
        prioridad = self.cleaned_data.get("prioridad")
        if prioridad == "" or prioridad == None:
            raise forms.ValidationError("El campo Servicio no puede quedar vacio")
        
    def validacion_descripcion(self):
        condicion = self.cleaned_data.get("condicion")
        if condicion == "" or condicion == None:
            raise forms.ValidationError("El campo Descripcion no puede quedar vacio")
                
class PrioridadFormulario(forms.Form):
    Prioridad = forms.CharField(max_length= 5, required=True)
    condicion = forms.CharField(max_length= 50, required=True)
    activo = forms.BooleanField(required= False)
    

#Nacionalidad
""" class NacionalidadModelForm(forms.ModelForm):
    
    class Meta:
        model = Nacionalidad 
        fields = ("pais")

    def validacion_pais(self):
        pais = self.cleaned_data.get("pais")
        if pais == "" or pais == None:
            raise forms.ValidationError("El campo Servicio no puede quedar vacio")
        
                
class PaisFormulario(forms.Form):
    Pais = forms.CharField(max_length= 40, required=True) """

#Tipo_documento
""" class TipoDocumentoModelForm(forms.ModelForm):
    
    class Meta:
        model = TipoDocumento 
        fields = ("nacionalidad, tipo_documento")

    def validacion_nacionalidad(self):
        nacionalidad = self.cleaned_data.get("nacionalidad, ")
        if nacionalidad == "" or nacionalidad == None:
            raise forms.ValidationError("El campo Servicio no puede quedar vacio")
        
                
class TipoDocumentoFormulario(forms.Form):
    Pais = forms.CharField(max_length= 40, required=True)
 """


