from django.shortcuts import render
from .models import Servicio
from .forms import ServicioFormulario, ServicioModelForm
# Create your views here.
def Inicio(request):
    form = ServicioModelForm(request.POST or None)
    if form.is_valid():
        print(form.data)
        form_data = form.cleaned_data
        print(form_data)
        aux1 = form.data.get("servicio")
        aux2 = form_data.get("descripcion")
        aux3 = form_data.get("activo")
        # obj = Servicio()
        # obj.servicio = aux1
        # obj.descripcion = aux2
        # obj.activo = aux3 
        # instance = obj.save(commit = false) Guardar sin commitear en la bd, con esto se puede realizar validaciones
        # if not instance.activo:
        #   instance.activo = True 
        # obj.save() Es el que envia en la base de datos
        obj = Servicio.objects.create(servicio = aux1, descripcion = aux2, activo = aux3)

       

    context = {
        "Titulo":'Servicio',
        "Formulario":form
    }

    return render(request,"Inicio.html", context)

def Dashboard(request):
     
     return render(request,"BaseDashboard.html")