# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


""" class AppEstado(models.Model):
    id = models.BigAutoField(primary_key=True)
    descripcion = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'app_estado' """

#Libreria postgres
class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    id = models.BigAutoField(primary_key=True)
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)

#libreria postgres

# Tablas de Django
class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'

#Tablas Django


#Tablas Turnero
class Nacionalidad(models.Model):
    id_nacionalidad = models.AutoField(primary_key=True, db_comment='CLAVE PRIMARIA DE LA TABLA NACIONALIDAD')
    pais = models.CharField(max_length=40, db_comment='PAIS DE NACIONALIDAD')

    class Meta:
        #managed = False
        db_table = 'nacionalidad'
        db_table_comment = 'SE GUARGA EL PAIS DE LA NACIONALIDAD'


class TipoDocumento(models.Model):
    id_tipo_documento = models.AutoField(primary_key=True, db_comment='CLAVE PRIMARIA TABLA TIPO DOCUMENTO')
    id_nacionalidad = models.ForeignKey('Nacionalidad', on_delete= models.CASCADE, db_column='id_nacionalidad', db_comment='NACIONALIDAD DEL DOCUMENTO')
    tipo_documento = models.CharField(max_length=20, db_comment='TIPO DE DOCUMENTO DE IDENTIDAD')

    class Meta:
        #managed = False
        db_table = 'tipo_documento'
        db_table_comment = 'TABLA DONDE SE GUARDA EL TIPO DE DOCUMENTO'

class Prestadora(models.Model):
    id_prestadora = models.AutoField(primary_key=True, db_comment='CLAVE PRIMARIA TABLA PRESTADORA')
    empresa = models.CharField(max_length=100, db_comment='NOMBRE DE LA EMPRESA PRESTADORA')
    activo = models.BooleanField(db_comment='ESTADO DE LA PRESTADORA\nACTIVO = TRUE\nINACTIVO = FALSE')

    class Meta:
        #managed = False
        db_table = 'prestadora'
        db_table_comment = 'TABLA DONDE SE GUARDAN LAS EMPRESAS PRESTADORAS DE SERVICIOS MOVILES'



class Persona(models.Model):
    id_persona = models.AutoField(primary_key=True, db_comment='CLAVE PRIMARIA TABLA PERSONA')
    nombres = models.CharField(max_length=50, db_comment='NOMBRE DE LA PERSONA')
    apellidos = models.CharField(max_length=50, db_comment='APELLIDOS DE PERSONAS')
    documento = models.CharField(max_length=20, db_comment='DOCUMENTO DE IDENTIDAD')
    id_tipo_documento = models.ForeignKey('TipoDocumento', on_delete= models.CASCADE, db_column='id_tipo_documento', db_comment='CLAVE FORANEA DE TIPO_DOCUMENTO')
    id_nacionalidad = models.ForeignKey('Nacionalidad', on_delete= models.CASCADE, db_column='id_nacionalidad', db_comment='NACIONALIDAD DE LA PERSONA')
    fecha_nacimiento = models.DateField(blank=True, null=True, db_comment='FECHA NACIMIENTO PERSONA')
    email = models.CharField(max_length=50, blank=True, null=True, db_comment='CORREO ELECTRONICO DE PERSONA')

    class Meta:
        #managed = False
        db_table = 'persona'
        db_table_comment = 'TABLA DE REGISTRO DE PERSONA'


class Telefono(models.Model):
    id_telefono = models.AutoField(primary_key=True, db_comment='CLAVE PRIMARIA TABLA TELEFONO')
    id_prestadora = models.ForeignKey('Prestadora', on_delete= models.CASCADE, db_column='id_prestadora', db_comment='CLAVE FORANEA DE PRESTADORAS DE SERVICIOS MOVILES')
    prefijo = models.CharField(max_length=4, db_comment='PREFIJO DE TELEFONOS\n021, 041, 0961, 0972...')
    numero = models.IntegerField(db_comment='NUMERO DE TELEFONO')
    principal = models.BooleanField(db_comment='CAMPO PARA RECONOCER EL TELEFONO PRINCIPAL\nPRINCIPAL = TRUE\nNO PRINCIPAL = FALSE')
    activo = models.BooleanField(db_comment='ESTADO DEL TELEFONO\nACTIVO = TRUE\nINACTIVO = TRUE')
    id_persona = models.ForeignKey('Persona', on_delete= models.CASCADE, db_column='id_persona', db_comment='CLAVE FORANEA DE CODIGO DE PERSONA')

    class Meta:
        #managed = False
        db_table = 'telefono'
        db_table_comment = 'TABLA DE TELEFONOS'


class Cliente(models.Model):
    id_cliente = models.AutoField(primary_key=True, db_comment='CLAVE PRIMARIA DE LA TABLA CLIENTES')
    id_persona = models.ForeignKey('Persona', on_delete= models.CASCADE, db_column='id_persona', db_comment='CLAVE FORANEA DE CODIGO DE PERSONA')
    activo = models.BooleanField(db_comment='ESTADO DEL CLIENTE\nACTIVO = TRUE\nINACTIVO = FALSE')
    fecha_alta = models.DateField(db_comment='FECHA DE ALTA DEL CLIENTE')
    fecha_baja = models.DateField(null=True, db_comment='FECHA EN EL QUE SE LE DA DE BAJA AL CLIENTE')

    class Meta:
        #managed = False
        db_table = 'cliente'
        db_table_comment = 'tabla de clientes'

""" class Usuario(models.Model):
    id_usuario = models.IntegerField(primary_key=True, db_comment='CLAVE PRIMARIA DE LA TABLA USUARIO')
    id_persona = models.ForeignKey('Persona', on_delete= models.CASCADE, db_column='id_persona', db_comment='CLAVE FORANEA CON LA TABLA PERSONA')
    usuario = models.CharField(max_length=20, db_comment='NOMBRE DE USUARIO DEL SISTEMA')
    clave = models.CharField(max_length=20, db_comment='PASSWORD DEL USUARIO')
    id_rol = models.ForeignKey('Rol', on_delete= models.CASCADE, db_column='id_rol', db_comment='CLAVE FORANEA CON LA TABLA ROL')
    fecha_alta = models.DateField(db_comment='FECHA DE ALTA DEL USUARIO')
    fecha_baja = models.DateField(db_comment='FECHA DE BAJA DEL USUARIO')
    activo = models.BooleanField(db_comment='ESTADO DEL REGISTRO USUARIO\nACTIVO = TRUE\nINACTIVO = FALSE')

    class Meta:
        #managed = False
        db_table = 'usuario'
        db_table_comment = 'TABLA DE USUARIOS DEL SISTEMA' """


class Cola(models.Model):
    id_cola = models.AutoField(primary_key=True, db_comment='CLAVE PRIMARIA DE LA TABLA COLA')
    id_usuario = models.ForeignKey('AuthUser', on_delete= models.CASCADE, db_column='id_usuario', db_comment='CLAVE FORANEA DE LA TABLA USUARIO')
    activo = models.BooleanField(db_comment='ESTADO DE LA COLA\nACTIVO = TRUE\nINACTIVO = FALSE')

    class Meta:
        #managed = False
        db_table = 'cola'
        db_table_comment = 'TABLA DE LAS COLAS'

class Prioridad(models.Model):
    id_prioridad = models.BigAutoField(primary_key=True, db_comment='CLAVE PRIMARAIA DE PRIORIDAD')
    prioridad = models.CharField(max_length=5, db_comment='PRIORIDAD PARA LA ATENCION EN LA COLA')
    condicion = models.CharField(max_length=50, db_comment='CONDICION REQUERIDA PARA INGRESAR EN LA PRIORIDAD')
    activo = models.BooleanField(db_comment='ESTADO DE LA PRIORIDAD\nACTIVO = TRUE\nINACTIVO = FALSE')

    class Meta:
        #managed = False
        db_table = 'prioridad'
        db_table_comment = 'TABLA DE PRIORIDAD PARA LA COLA'

class Servicio(models.Model):
    id_servicio = models.BigAutoField(primary_key=True, db_comment='CLAVE PRIMARIA DE TABLA SERVICIO')
    servicio = models.CharField(max_length=50, db_comment='NOMBRE DEL SERVICIO')
    descripcion = models.CharField(max_length=100, db_comment='DESCRIPCION DEL SERVICIO')
    activo = models.BooleanField(db_comment='ESTADO DEL SERVICIO\nACTIVO = TRUE\nINACTIVO = FALSE')

    class Meta:
        #managed = False
        db_table = 'servicio'
        db_table_comment = 'TABLA DE LOS SERVICIOS PARA LOS CLIENTES'


class Ticket(models.Model):
    id_ticket = models.AutoField(primary_key=True, db_comment='CLAVE PRIMARIA DE LA TABLA TICKET')
    id_cliente = models.ForeignKey('Cliente', on_delete= models.CASCADE, db_column='id_cliente', db_comment='CLAVE FORANEA DE CLIENTE')
    id_prioridad = models.ForeignKey('Prioridad', on_delete= models.CASCADE, db_column='id_prioridad', db_comment='CLAVE FORANEA DE PRIORIDAD')
    id_servicio = models.ForeignKey('Servicio', on_delete= models.CASCADE, db_column='id_servicio', db_comment='CLAVE FORANEA DE SERVICIO')

    class Meta:
        #managed = False
        db_table = 'ticket'
        db_table_comment = 'TABLA DE TICKETS GENERADOS'


class ColaTicket(models.Model):
    id_cola_ticket = models.AutoField(primary_key=True, db_comment='CLAVE PRIMARIA DE LA TABLA COLA_TICKET')
    id_ticket = models.ForeignKey('Ticket', on_delete= models.CASCADE, db_column='id_ticket', db_comment='CLAVE FORANEA DE LA TABLA TICKET')
    id_cola = models.ForeignKey('Cola', on_delete= models.CASCADE, db_column='id_cola', db_comment='CLAVE FORANEA DE LA TABLA COLA')
    fecha_hora_atencion = models.DateTimeField(db_comment='FECHA Y HORA DE ATENCION')
    fecha_hora_finalizacion = models.DateTimeField(db_comment='FECHA Y HORA DE FINALIZACION')

    class Meta:
        #managed = False
        db_table = 'cola_ticket'
        db_table_comment = 'TABLA ENLACE ENTRE LA COLA Y EL TICKET DEL CLIENTE'



""" class Rol(models.Model):
    id_rol = models.BigAutoField(primary_key=True, db_comment='CLAVE PRIMARIA DE ROLES')
    rol = models.CharField(max_length=50, db_comment='NOMBRE DEL ROL')
    descripcion = models.CharField(max_length=200, db_comment='BREVE DESCRIPCION DEL ROL')
    activo = models.BooleanField(db_comment='ESTADO DEL ROL\nACTIVO = TRUE\nINACTIVO= FALSE')

    class Meta:
        #managed = False
        db_table = 'rol'
        db_table_comment = 'TABLA DE ROLES PARA LOS USUARIOS'


class Permiso(models.Model):
    id_permiso = models.AutoField(primary_key=True, db_comment='CLAVE PRIMARIA DE LA TABLA PERMISO')
    permiso = models.CharField(max_length=50, db_comment='NOMBRE DEL PERMISO')
    activo = models.BooleanField(db_comment='ESTADO DEL PERMISO\nACTIVO = TRUE\nINACTIVO = FALSE')

    class Meta:
        #managed = False
        db_table = 'permiso'
        db_table_comment = 'TABLA DE PERMISOS'


class RolPermiso(models.Model):
    id_rol_permiso = models.AutoField(primary_key=True, db_comment='CLAVE PRIMARIA DE LA TABLA ROL_PERMISO')
    id_rol = models.ForeignKey('Rol', on_delete= models.CASCADE, db_column='id_rol', db_comment='CLAVE FORANEA DE LOS ROLES')
    id_permiso = models.ForeignKey('Permiso', on_delete= models.CASCADE, db_column='id_permiso', db_comment='CLAVE FORANEA DE PERMISOS')
    fecha_alta = models.DateField(db_comment='FECHA DE ALTA')

    class Meta:
        #managed = False
        db_table = 'rol_permiso'
        db_table_comment = 'TABLA ENLACE ENTRE LOS PERMISOS Y LOS ROLES' """











