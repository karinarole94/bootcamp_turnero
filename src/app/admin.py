from django.contrib import admin
from . import models

# Register your models here.
class ServicioAdmin(admin.ModelAdmin):
    list_display = ["servicio","descripcion","activo"]
    list_filter = ["activo"]
    list_editable = ["descripcion"]
    search_fields = ["servicio"]

class PrioridadAdmin(admin.ModelAdmin):
    list_display = ["prioridad","condicion","activo"]
    list_filter = ["activo"]
    list_editable = ["condicion"]
    search_fields = ["prioridad"]

class NacionalidadAdmin(admin.ModelAdmin):
    list_display = ["pais"]
    #list_editable = ["pais"]
    search_fields = ["pais"]    

class PrestadoraAdmin(admin.ModelAdmin):
    list_display = ["empresa","activo"]
    list_filter = ["activo"]
    #list_editable = ["empresa"]
    search_fields = ["empresa"]

class TipoDocumentoAdmin(admin.ModelAdmin):
    list_display = ["id_nacionalidad","tipo_documento"]
    #list_filter = ["activo"]
    list_editable = ["tipo_documento"]
    search_fields = ["tipo_documento"]    

class PersonaAdmin(admin.ModelAdmin):
    list_display = ["nombres","apellidos", "documento","id_tipo_documento","id_nacionalidad","fecha_nacimiento","email"]
    #list_filter = ["activo"]
    #list_editable = ["nombres","apellidos"]
    search_fields = ["nombres","apellidos"]    

class TelefonoAdmin(admin.ModelAdmin):
    list_display = ["id_prestadora","prefijo","numero","principal", "activo","id_persona"]
    list_filter = ["principal","activo"]
    list_editable = ["prefijo","numero"]
    search_fields = ["persona"]

class ClienteAdmin(admin.ModelAdmin):
    list_display = ["id_persona","activo","fecha_alta","fecha_baja"]
    list_filter = ["activo"]
    #list_editable = ["fecha_baja"]
    search_fields = ["id_persona"]

class TicketAdmin(admin.ModelAdmin):
    list_display = ["id_cliente","id_prioridad","id_servicio"]
    #list_filter = ["activo"]
    #list_editable = ["prefijo","numero"]
    search_fields = ["id_persona"]  
    

class ColaAdmin(admin.ModelAdmin):
    list_display = ["id_usuario","activo"]
    list_filter = ["activo"]
    #list_editable = ["prefijo","numero"]
    search_fields = ["id_usuario"]

class ColaticketAdmin(admin.ModelAdmin):
    list_display = ["id_ticket","id_cola","fecha_hora_atencion","fecha_hora_finalizacion"]
    #list_filter = ["activo"]
    #list_editable = ["prefijo","numero"]
    search_fields = ["id_ticket"]

 
admin.site.register(models.Servicio, ServicioAdmin)
admin.site.register(models.Prioridad, PrioridadAdmin)
admin.site.register(models.Nacionalidad, NacionalidadAdmin)
admin.site.register(models.Prestadora, PrestadoraAdmin)
admin.site.register(models.TipoDocumento, TipoDocumentoAdmin)
admin.site.register(models.Persona, PersonaAdmin)
admin.site.register(models.Telefono, TelefonoAdmin)
admin.site.register(models.Cliente, ClienteAdmin)
admin.site.register(models.Ticket, TicketAdmin)
admin.site.register(models.Cola, ColaAdmin)
admin.site.register(models.ColaTicket, ColaticketAdmin)